# Stopper

Stopper is a utility to kill all instances of stuck processes. To use the end-user version of
stopper, simply navigate to its directly and launch `stopper.exe`. This will launch the frontend
and its backend. Type in all or part of an executable name (or class of executables) and click Go!

Stopper will then take care of the rest.

If you're interested in using stopper for its backend, you can communicate to the backend
via port `12000` using the WebSocket protocol. Connections are naively managed given the
expected single connection, and close following one successful transaction.

The client is written to expect this, and will appropriately go into the 'loading state' while it
is reconnecting.

Presently it only works for Windows (due to the win32 dependency for fine grained access)
but future versions will provide non-MacOS Unix support (the utility isn't needed for MacOS,
force quit is obtainable via right clicking dock icons)

This is GPLv3 licensed software, and the license terms may be seen in the LICENSE file.