package build

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const FrontEndRoot = "./frontend"
const FrontEndDist = "./frontend/dist"
const FrontEndMainName = "main.js"
const FrontEndMain = "frontend/main.js"
const FrontEndName = "stopper-frontend"
const CombinedLocation = "./frontend/stopper-frontend-win32-x64"
const BootstrapEntry = "stopper.go"
const BackendEntry = "./backend/stopper-backend.go"
const BackendBuild = "stopper-backend.exe"
const BootstrapBuild = "stopper.exe"
const FrontEndBundle = FrontEndDist + "/bundle.js"
const FrontEndDistPackageJson = FrontEndDist + "/package.json"
const RelativeDist = "./dist"
const CurrentOutDir = "./frontend/stopper-frontend-win32-x64"
const InstallerName = "./frontend/stopper.exe"
const StopperLicenseName = "LICENSE.txt"
const StopperDependencyLicenses = "LICENSES.stopper.dependencies.txt"