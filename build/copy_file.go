package build

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"fmt"
	"os"
)

func openSourceFile(name string) *os.File {
	file, err := os.Open(name)
	if err != nil {
		fmt.Println("Error opening file: ")
		panic(err)
	}
	return file
}

//defaults to 50 MB
func readSourceFileContents(file *os.File, size *int) []byte {
	var content []byte
	if size != nil {
		content = make([]byte, *size)
	} else {
		content = make([]byte, 50 * 1024 * 1024)
	}
	bytesRead, err := file.Read(content)
	if err != nil {
		fmt.Println("Error reading file: " + file.Name())
		panic(err)
	}
	return content[:bytesRead]
}

func waitForDirectory(path string) {
		for {
			fmt.Println("Waiting for directory to exist")
			_, err := os.Stat(path)
			if !os.IsNotExist(err) {
				break
			}
		}
}

func createDestination(path string) *os.File {
	fmt.Println("Creating copy at: " + path)
	file, err := os.Create(path)
	if err != nil {
		fmt.Println("Error creating executable at: " + path)
		panic(err)
	}
	return file
}

func writeBytesToCopy(file *os.File, content []byte) {
	fmt.Println("Writing bytes to new file: " + file.Name())
	bytesWritten, err := file.Write(content)
	if err != nil {
		fmt.Println("Error writing bytes to copied file: " + file.Name())
		panic(err)
	} else {
		fmt.Printf("%s was %d bytes!\n", file.Name(), bytesWritten)
	}
}

func cleanShop(origFile *os.File, newFile *os.File) {
	fmt.Println("Cleaning up like a good gopher!")
	_ = origFile.Close()
	_ = newFile.Close()
}

func CopyFile(source string, dest string, destDirectoryToStat string) {
	src := openSourceFile(source)
	contents := readSourceFileContents(src, nil)
	waitForDirectory(destDirectoryToStat)
	target := createDestination(dest)
	writeBytesToCopy(target, contents)
	cleanShop(src, target)
}