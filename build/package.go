package build

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"fmt"
	"os/exec"
)

func createPackageCommand(source string, dist string) *exec.Cmd {
	cmd := exec.Command("7z", "a", "-sfx", dist, source)
	return cmd
}

func executeCommand(cmd *exec.Cmd) {
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Could not create archive for reason: " + string(out))
		panic(err)
	}
}

func Package(source string, dist string) {
	command := createPackageCommand(source, dist)
	executeCommand(command)
}
