package build

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"fmt"
	"os/exec"
)

func initYarn(args []string, workingDir *string) *exec.Cmd {
	cmd := exec.Command("yarn", args...)
	if workingDir == nil {
		cmd.Dir = FrontEndRoot
	} else {
		cmd.Dir = *workingDir
	}
	return cmd
}

func runYarnSync(cmd *exec.Cmd) {
	fmt.Println("Running yarn")
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Error encountered while attempting to run yarn: ")
		fmt.Println(string(out))
		panic(err)
	}
}

func RunYarn(args []string, workingDir *string) {
	cmd := initYarn(args, workingDir)
	runYarnSync(cmd)
}