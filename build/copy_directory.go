package build

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func dosifyPath(path string) string {
	cwd, _ := os.Getwd()
	return cwd + "\\" + strings.Replace(strings.Replace(path, "./", "", -1), "/", "\\", -1)
}

func escapePath(path string) string {
	return "\"" + path + "\""
}

func createCommand(source string, dest string) *exec.Cmd {
	if runtime.GOOS == "windows" {
		//this is a great utility but... compared to below - why did Windows need to be different just to be different?
		safeSource := dosifyPath(source)
		safeDest := dosifyPath(dest)
		fmt.Printf("Windows detected, using xcopy with altered params: %s %s\n", safeSource, safeDest)
		return exec.Command("xcopy", safeSource, safeDest, "/e", "/q", "/y", "/i")
	} else {
		fmt.Println("POSIX detected, using cp")
		return exec.Command("cp", "-r", escapePath(source), escapePath(dest))
	}
}

func executeCommandSync(command *exec.Cmd) {
	bytes, err := command.CombinedOutput()
	if err != nil {
		fmt.Println("Could not start process for reason:")
		fmt.Println(string(bytes))
		panic(err)
	}
}

func CopyDirectory(source string, dest string) {
	cmd := createCommand(source, dest)
	executeCommandSync(cmd)
}
