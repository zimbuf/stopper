package net

import (
	"../log"
	"../sys"
	"./context"
	"github.com/gorilla/websocket"
	"net/http"
	"os"
	"strconv"
)

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.0
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const END_OF_TRANSMISSION = "STOPPER://EOT"

func processRequest(bytes []byte, log *log.Log, stream *websocket.Conn) {
	unsafeProcessName := string(bytes)
	processName := unsafeProcessName[1:len(unsafeProcessName)-1]
	logger := *log
	if processName != END_OF_TRANSMISSION {
		logger.Write("Received process name: " + processName)
		context.SetRequestContext(processName, stream)
		sys.KillProcess(processName, log)
	} else {
		logger.Write("End of transmission received. Closing backend")
		os.Exit(0)
	}
}

func makeMessageTypeReadable(messageType int) string {
	switch messageType {
	case websocket.TextMessage:
		return "Text Message"
	case websocket.BinaryMessage:
		return "Binary Message"
	case websocket.CloseMessage:
		return "Close Message"
	case websocket.PingMessage:
		return "Ping Message"
	case websocket.PongMessage:
		return "Pong Message"
	default:
		return "Unknown Message: " + strconv.Itoa(messageType)
	}
}

func determineActionBasedOnType(messageType int, bytes []byte, log *log.Log, stream *websocket.Conn) {
	logger := *log
	switch messageType {
	case websocket.TextMessage:
		logger.Write("Text message received")
		processRequest(bytes, log, stream)
		break
	default:
		logger.Write("Unsupported message type received: " + makeMessageTypeReadable(messageType))
		break
	}
}

func readRequestIfNoErr(stream *websocket.Conn, log *log.Log) {
	logger := *log
	messageType, bytes, err := stream.ReadMessage()
	if err != nil {
		logger.Write(err.Error())
	} else {
		logger.Write("Received request and message")
		determineActionBasedOnType(messageType, bytes, log, stream)
	}
}

func handleFrontendRequestsFactory(log *log.Log) func(w http.ResponseWriter, r *http.Request) {
	logger := *log
	return func(w http.ResponseWriter, r *http.Request){
		upgrader := GetUpgrader()
		stream, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			logger.Write(err.Error())
		} else {
			logger.Write("Received connection")
			readRequestIfNoErr(stream, log)
		}
	}
}

func StartServer(log *log.Log) {
	logger := *log
	http.HandleFunc("/", handleFrontendRequestsFactory(log))
	err := http.ListenAndServe(":12000", nil)
	if err != nil {
		logger.Write(err.Error())
	}
}
