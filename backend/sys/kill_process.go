package sys

import (
	"../log"
	"../net/context"
	"container/list"
	"github.com/JamesHovious/w32"
	"github.com/gorilla/websocket"
	"regexp"
	"syscall"
	"strings"
)
/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

func safelyGetStringFromExeInt(proc *w32.PROCESSENTRY32, log *log.Log) string {
	const MaxFilePathCharacters = 250
	const NulByte = "\x00"
	logger := *log
	unsafeExeName := syscall.UTF16ToString(proc.ExeFile[:MaxFilePathCharacters])
	logger.Write("Retrieved exe name: " + unsafeExeName)
	return strings.Trim(unsafeExeName, NulByte)
}

func getSnapshot() w32.HANDLE {
	const Process = 3
	const SysPid = 0
	return w32.CreateToolhelp32Snapshot(Process, SysPid)
}

func loopThroughProcessesForMatchingName(snapshot *w32.HANDLE, name string, log *log.Log) *list.List {
	results := list.New()
	proc := new(w32.PROCESSENTRY32)
	expression := regexp.MustCompile("(?i)" + name)
	logger := *log
	for w32.Process32Next(*snapshot, proc) {
		exeName := safelyGetStringFromExeInt(proc, log)
		if expression.MatchString(exeName) {
			logger.Write("Matched process: " + exeName)
			results.PushBack(*proc)
		} else {
			logger.Write("Did not match process: " + exeName)
		}
	}
	return results
}

func getProcessHandles(procs *list.List, log *log.Log) *list.List {
	var proc = procs.Front()
	handles := list.New()
	logger := *log
	for proc != nil {
		handle, err := w32.OpenProcess(w32.PROCESS_ALL_ACCESS, false, proc.Value.(w32.PROCESSENTRY32).ProcessID)
		if err == nil {
			logger.Write("Pushing back handle: " + string(handle))
			handles.PushBack(handle)
		} else {
			logger.Write("Could not get handle for reason: " + err.Error())
		}
		proc = proc.Next()
	}
	return handles
}

func terminateProcesses(handles *list.List, log *log.Log) {
	var handle = handles.Front()
	logger := *log
	for handle != nil {
		realHandle := handle.Value.(w32.HANDLE)
		logger.Write("Terminating process: " + string(realHandle))
		w32.TerminateProcess(realHandle, 0)
		handle = handle.Next()
	}
}

func cleanUpHandles(handles *list.List, log *log.Log) {
	var handle = handles.Front()
	logger := *log
	for handle != nil {
		realHandle := handle.Value.(w32.HANDLE)
		logger.Write("Closing process: " + string(realHandle))
		w32.CloseHandle(realHandle)
		handle = handle.Next()
	}
}

func sendResponse(processName string, log *log.Log) {
	logger := *log
	connection := context.GetRequestContext(processName)
	if connection != nil {
		logger.Write("sending back 'PROCKILL' for process name: " + processName)
		err := connection.WriteMessage(websocket.TextMessage, []byte("PROCKILL"))
		if err != nil {
			logger.Write("Could not return response to client for reason: " + err.Error())
		}
		closeErr := connection.Close()
		if closeErr != nil {
			logger.Write("Could not close connection for reason: " + closeErr.Error())
		} else {
			logger.Write("Closed connection")
		}
	} else {
		logger.Write("Could not find connection for process name " + processName)
	}
}

func KillProcess(processName string, log *log.Log) {
	snapshot := getSnapshot()
	procs := loopThroughProcessesForMatchingName(&snapshot, processName, log)
	handles := getProcessHandles(procs, log)
	terminateProcesses(handles, log)
	cleanUpHandles(handles, log)
	sendResponse(processName, log)
}
