// +build mage

package main

import (
	"./build"
)

/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

func Build() {
	build.RunYarn([]string{"webpack"}, nil)
	build.Compile([]string{"-v", build.BootstrapEntry})
	build.Compile([]string{"-ldflags", "-H=windowsgui", "-o", build.BackendBuild, build.BackendEntry})
}

func Dist() {
	dist := build.FrontEndDist
	build.CopyFile(build.FrontEndRoot + "/package.json", build.FrontEndDistPackageJson, build.FrontEndDist)
	build.CopyFile(build.FrontEndMain, build.FrontEndDist + "/" + build.FrontEndMainName, build.FrontEndDist)
	build.RunYarn([]string{"install"}, &dist)
	build.RunYarn([]string{"electron-packager", build.RelativeDist, build.FrontEndName, "--platform=win32", "--arch=x64"}, nil)
	build.CopyFile(build.BootstrapBuild, build.CombinedLocation + "/" + build.BootstrapBuild, build.CombinedLocation)
	build.CopyFile(build.BackendBuild, build.CombinedLocation + "/" + build.BackendBuild, build.CombinedLocation)
	build.RenameElectronLicense()
	build.CopyFile(build.StopperLicenseName, build.CurrentOutDir + "/" + build.StopperLicenseName, build.CombinedLocation)
	build.CopyFile(build.StopperDependencyLicenses, build.CurrentOutDir + "/" + build.StopperDependencyLicenses, build.CombinedLocation)
	build.Package(build.CurrentOutDir, build.InstallerName)
}

func Clean() {
	build.DeleteAll(build.CombinedLocation)
	build.DeleteAll(build.BackendBuild)
	build.DeleteAll(build.BootstrapBuild)
	build.DeleteAll(build.FrontEndDist + "/" + build.FrontEndMainName)
	build.DeleteAll(build.FrontEndDist + "/" + "yarn.lock")
	build.DeleteAll(build.InstallerName)
	build.DeleteAll(build.FrontEndDist + "/node_modules")
	build.DeleteAll(build.FrontEndDistPackageJson)
	build.DeleteAll(build.FrontEndBundle)
}