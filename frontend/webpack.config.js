const { resolve } = require('path');

module.exports = {
    mode: 'development',
    entry: resolve(__dirname, "react/app.jsx"),
    output: {
        path: resolve(__dirname, "dist"),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /.webp/,
                exclude: /node_modules/,
                include: resolve(__dirname, "react"),
                use: {
                    loader: 'file-loader'
                }
            },
            {
                test: /.jsx?$/,
                exclude: /node_modules/,
                include: resolve(__dirname, "react"),
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            ["@babel/plugin-transform-modules-commonjs"]
                        ],
                        presets: [
                            ['@babel/preset-react'],
                            ['@babel/preset-env']
                        ]
                    }
                }
            }
        ]
    },
    devServer: {
        contentBase: resolve(__dirname, "dist")
    }
};