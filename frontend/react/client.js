/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

let socket;
let ready;

const setReady = (value) => () => {
    console.log(`Setting server status to: ${value}`);
    ready = value;
};

export const isReady = () => {
    console.log(`Server is: ${ready}`);
    return ready;
};

export const flush = () => {
    setTimeout(() => {
        console.log("Killing connection");
        setReady(false)();
        startSocket();
    }, 5000);
};

const startSocket = () => {
    setTimeout(() => {
        console.log("Starting socket");
        socket = new WebSocket("ws://localhost:12000/");
        socket.onopen = setReady(true);
        socket.onclose = flush;
    }, 5000);

};

export const sendProcessName = (processName) => {
    const timer = setInterval(() => {
        if (socket && socket.readyState === 1) {
            socket.send(processName);
            clearInterval(timer);
        }
    }, 2000);
};

startSocket();