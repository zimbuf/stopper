/*
 *  Copyright (C) 2019  zimbuf
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import React from 'react';
import ReactDOM from 'react-dom';
import styled from '@emotion/styled';
import loadingImage from './arc2.webp';
import { isReady, sendProcessName } from "./client";

const Banner = styled('span')`
  width:200px;
  height:50px;
  font-size:35px;
  align-items:center;
  color:#2ecc71;
  justify-content:center;
  font-family:sans-serif;
  font-weight:bold;
`;

const LoadingContainer = styled('div')`
  width:240px;
  height:50px;
  align-self:right;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

const Loading = styled('img')`
  height:50px;
  width:50px;
  display: flex;
`;

const Submit = styled('button')`
  width:35px;
  height:35px;
  padding-right:3px;
  border-color:#82e0aa;
  background-image:linear-gradient(to bottom, #2ecc71,  #196f3d);
  font-size:12px;
  align-items:center;
  font-family:sans-serif;
  font-weight:bold;
  display:inline-flex;
`;

const Input = styled('input')`
  width:200px;
  height:35px;
  font-size:12px;
  color:#2ecc71;
  background-color:#212f3c;
  font-family:sans-serif;
  border:none;
  font-weight:bold;
  display:inline-flex;
`;

const Label = styled('span')`
  width:200px;
  height:35px;
  font-size:12px;
  vertical-align:center;
  color:#2ecc71;
  font-family:sans-serif;
  font-weight:bold;
  align-items:center;
  display:inline-flex;
`;

const Page = styled('div')`
  width:440px;
  height:100px;
  padding:2.5px;
  background-color:#17202a;
  flex-flow: row wrap;
  display:flex;
  justify-content:center;
`;

const waitForCompletion = (setWaiting) => setTimeout(() => setWaiting(false), 10000);

const execStopper = (setWaiting, processName) => {
    setWaiting(true);
    sendProcessName(JSON.stringify(processName));
    waitForCompletion(setWaiting);
};

const checkServerStatus = (setWaiting) => setInterval(() => setWaiting(isReady()), 2000);

const App = () => {
    const [waiting, setWaiting] = React.useState(false);
    const [processName, setProcessName] = React.useState('');
    const [ready, setReady] = React.useState(false);
    React.useEffect(() => {
        checkServerStatus(setReady)
    }, []);
    return (
        <Page>
            <Banner>Stopper</Banner><LoadingContainer >{(waiting || !ready) && <Loading src={loadingImage} />}</LoadingContainer>
            <Label>Process Name or Part of Name:</Label>
            <Input readOnly={waiting || !ready} onChange={event => setProcessName(event.target.value)} />
            <Submit disabled={waiting|| !ready} onClick={() => execStopper(setWaiting, processName)} >Go!</Submit>
        </Page>
    );
};

ReactDOM.render(<App />, document.getElementById("root"));